<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package magazie-minimal
 */

global $magazie_minimal_theme_options;
 $designlayout = $magazie_minimal_theme_options['magazie-minimal-layout'];

if ( !is_active_sidebar( 'sidebar-1' ) || 'no-sidebar' == $designlayout || 'right-sidebar' == $designlayout ) {
	return;
}

if( 'left-sidebar' == $designlayout || 'both-sidebar' == $designlayout ){
	$side_col = 'left-sidebar';

?>

<aside id="secondary" class="widget-area col-sm-3 col-md-3 <?php echo esc_attr($side_col); ?>" role="complementary">
	<?php dynamic_sidebar( 'sidebar-1' ); ?>
</aside><!-- #secondary -->
<?php } ?>