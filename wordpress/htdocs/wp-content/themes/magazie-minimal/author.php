<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package magazie-minimal
 */
get_header(); ?>
				    <div id="primary" class="content-area col-sm-9 col-md-9 pull-right">
						<main id="main" class="site-main" role="main">
								<?php
								if ( have_posts() ) : ?>
									<header class="page-header">
										<?php
											the_archive_title( '<h1 class="page-title">', '</h1>' );
										get_template_part( 'template-parts/author', 'archive' );

										?>
									</header><!-- .page-header -->
				                   <?php
									/* Start the Loop */
									while ( have_posts() ) : the_post();

										/*
										 * Include the Post-Format-specific template for the content.
										 * If you want to override this in a child theme, then include a file
										 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
										 */
										get_template_part( 'template-parts/content', get_post_format() );

									endwhile;

									the_posts_pagination( array(
										'mid_size' => 6,
										'prev_text' => esc_html__( 'Prev', 'magazie-minimal' ),
										'next_text' => esc_html__( 'Next', 'magazie-minimal' ),
									) );

								else :

									get_template_part( 'template-parts/content', 'none' );

								endif; ?>
						</main><!-- #main -->
					</div><!-- #primary -->
                	<?php get_sidebar(); ?>
				</div><!-- #content -->
			</div><!-- col-9 -->
			<div class="col-sm-3 col-md-3">
				<?php get_sidebar('right'); ?>
			</div>
	</div><!-- #row -->
</div><!-- #page -->
<?php get_footer(); ?>
