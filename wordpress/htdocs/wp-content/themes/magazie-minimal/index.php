<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package magazie-minimal
 */

get_header(); ?>

				<div id="primary" class="content-area col-sm-9 col-md-9 pull-right">
					<main id="main" class="site-main" role="main">
						<?php
						if ( have_posts() ) :

							if ( is_home() && ! is_front_page() ) : ?>
								<header>
									<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
								</header>

							<?php
							endif;
							/* Start the Loop */
							while ( have_posts() ) : the_post();

								/*
								 * Include the Post-Format-specific template for the content.
								 * If you want to override this in a child theme, then include a file
								 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
								 */
								get_template_part( 'template-parts/content', get_post_format() );

							endwhile;

							the_posts_pagination( array(
								'mid_size' => 2,
								'prev_text' => esc_html__( 'Prev', 'magazie-minimal' ),
								'next_text' => esc_html__( 'Next', 'magazie-minimal' ),
							) );

						else :

							get_template_part( 'template-parts/content', 'none' );

						endif; ?>
					</main><!-- #main -->
					</div><!-- #primary -->
					<?php get_sidebar(); ?>
					</div><!-- #content -->
				</div><!-- col-9 -->
		<div class="col-sm-3 col-md-3 no-p-l">
			<?php get_sidebar('right'); ?>
		</div>
	</div><!-- #row -->
</div><!-- #page -->

			<?php get_footer(); ?>
