<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package magazie-minimal
 */
$share_url = urlencode(get_permalink( get_the_ID() ))
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<!--post thumbnal options-->
	<div class="post-thumb">
		<a href="<?php the_permalink(); ?>">
			<?php the_post_thumbnail( 'large' ); ?>
		</a>
	</div><!-- .post-thumb-->
   
	<header class="entry-header">
	<a href="<?php the_permalink(); ?>" class="tile_part"><?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</a>
	<?php
		if ( 'post' === get_post_type() ) : ?>
			<div class="entry-meta">
				<?php
				magazie_minimal_posted_author();
				magazie_minimal_posted_on();
				?>
			</div><!-- .entry-meta -->
			<?php
		endif; ?>
	</header><!-- .entry-header -->
	<div class="entry-content">
			<p>
			<?php
									/* translators: %s: Name of current post. */
					the_excerpt();
				

				wp_link_pages( array(
					'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'magazie-minimal' ),
					'after'  => '</div>',
				) );
			?>
			</p>
		<p> <a href="<?php the_permalink(); ?>" class="more-link"><?php echo esc_html__('Read More ','magazie-minimal') ?><span class="meta-nav"><?php echo esc_html__('&#8594','magazie-minimal') ?></span></a></p>

	</div><!-- .entry-content -->
		
</article><!-- #post-## -->