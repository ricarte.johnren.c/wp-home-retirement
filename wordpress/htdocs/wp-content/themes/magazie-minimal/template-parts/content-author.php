<?php
/**
 * Template part for author info
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package  magazie-minimal Themes
 */
?>
<div class="authorbox <?php echo ( 1 != get_option( 'show_avatars' ) ) ? 'no-author-avatar' : ''; ?>">
	  <?php if ( 1 == get_option('show_avatars') ): ?>
	  <div class="author-avatar">
	    <?php echo get_avatar( get_the_author_meta( 'user_email' ), '120', '' ); ?>
	  </div>
	  <?php endif ?>
	  <div class="author-info">
	    <h4 class="author-header"><?php  the_author_posts_link(); ?></h4>
	    <p class="author-content"><?php the_author_meta('description'); ?></p>
	  </div>
</div>