<?php
$post_id =get_the_ID();
$categories = get_the_category( $post_id );
$category = get_categories( $post_id );

if ($categories) {
    $category_ids = array();
    foreach ($categories as $category) {
        $category_ids[] = $category->term_id;
        $count = $category->category_count;
    }
    ?>
    <div class="related-post-wrapper">
    <?php
    if($count>1)
    {
    ?>
        <h2 class="widget-title">
            <?php esc_html_e('Related Articles', 'magazie-minimal'); ?>
        </h2>
   <?php } ?>
        <div class="row">
            <?php
            $read_more_cat_post_args = array(
                'category__in'       => $category_ids,
                'post__not_in'       => array($post_id),
                'post_type'          => 'post',
                'posts_per_page'     => 2,
                'post_status'        => 'publish',
                'ignore_sticky_posts'=> true
            );
            $read_more_featured_query = new WP_Query( $read_more_cat_post_args );

            while ( $read_more_featured_query->have_posts() ) : $read_more_featured_query->the_post();
                ?>
                <div class="blog-item col-sm-6">
                    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                        <div class="content-wrapper">
                            <?php
                            if ( has_post_thumbnail() ) {
                                ?>
                                <!--post thumbnal options-->
                                <a href="<?php the_permalink(); ?>" class="full-image">
                                    <?php
                                    the_post_thumbnail( 'small', array( 'class' => 'aligncenter' ) );
                                    ?>
                                </a>

                                <?php
                            }
                            ?>
                            <header class="entry-header">
                                <?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
                            </header><!-- .entry-header -->
                        </div>

                    </article><!-- #post-## -->
                </div>
                <?php
            endwhile;
            wp_reset_postdata();
            ?>
        </div>
    </div>
    <?php
}