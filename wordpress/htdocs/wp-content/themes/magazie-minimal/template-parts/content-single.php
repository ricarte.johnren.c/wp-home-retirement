<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package magazie-minimal
 */

$share_url = urlencode(get_permalink( get_the_ID() ))
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
   <!--post thumbnal options-->
   <div class="post-thumb">
	    <a href="<?php the_permalink(); ?>">
	     <?php the_post_thumbnail( 'large' ); ?>
	    </a>
   </div><!-- .post-thumb-->
	<header class="entry-header">
		<?php
	    	the_title( '<h1 class="entry-title">', '</h1>' );
		if ( 'post' === get_post_type() ) : ?>
			<div class="entry-meta">
				<?php
				magazie_minimal_posted_on();
				magazie_minimal_posted_author();

				?>
			</div><!-- .entry-meta -->
			<?php
		endif; ?>
	</header><!-- .entry-header -->
	<div class="entry-content">
			<?php
				the_content(); 

				wp_link_pages( array(
					'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'magazie-minimal' ),
					'after'  => '</div>',
				) );
			?>
	</div><!-- .entry-content -->
	<?php
		magazie_minimal_entry_footer();
		get_template_part( 'template-parts/content', 'author' );
		get_template_part( 'template-parts/related', 'posts' );
	?>
	<div class="comment-share">
		<div class="total-comment">
			<h3><?php comments_number( '0 Comment', '1 Comment', '% Comments' ); ?></h3>
		</div>

	</div>
</article><!-- #post-## -->