<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package magazie-minimal
 */
get_header(); ?>
				<div id="primary" class="content-area col-sm-9 col-md-9 pull-right">
					<main id="main" class="site-main" role="main">
			        	<?php
						 while ( have_posts() ) : the_post();
                             
							get_template_part( 'template-parts/content','single');

							the_post_navigation( array(
								'next_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Next', 'magazie-minimal' ) . '</span> ' .
									'<span class="screen-reader-text">' . __( 'Next post:', 'magazie-minimal' ),
								'prev_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Previous', 'magazie-minimal' ) . '</span> ' .
									'<span class="screen-reader-text">' . __( 'Previous post:', 'magazie-minimal' ),
							) );

							// If comments are open or we have at least one comment, load up the comment template.
							if ( comments_open() || get_comments_number() ) :
								comments_template();
							endif;

						  endwhile; // End of the loop.
						?>

					</main><!-- #main -->
				</div><!-- #primary -->
				<?php get_sidebar(); ?>
			</div><!-- #content -->
		</div><!-- col-9 -->
		<div class="col-sm-3 col-md-3 no-p-l">
			<?php get_sidebar('right'); ?>
	    </div>
	</div><!-- #row -->
</div><!-- #page -->
<?php get_footer(); ?>
