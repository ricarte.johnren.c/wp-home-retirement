<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package magazie-minimal
 */

global $magazie_minimal_theme_options;
$designlayout = $magazie_minimal_theme_options['magazie-minimal-layout'];

if ( ! is_active_sidebar( 'sidebar-right' ) || 'no-sidebar' == $designlayout || 'left-sidebar' == $designlayout ) {
	return;
}

if( 'right-sidebar' == $designlayout || 'both-sidebar' == $designlayout ){
	$side_col = 'right-sidebar';


?>

<aside id="secondary" class="widget-area bg-grey <?php echo esc_attr($side_col); ?>" role="complementary">
	<?php dynamic_sidebar( 'sidebar-right' ); ?>
	
</aside><!-- #secondary -->
<?php } ?>