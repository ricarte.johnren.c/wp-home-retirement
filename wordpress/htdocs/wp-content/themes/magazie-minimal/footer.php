<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package magazie-minimal
 */
 global $magazie_minimal_theme_options;
  $copyright= wp_kses_post($magazie_minimal_theme_options['magazie-minimal-footer-copyright']);
  if(!empty($copyright))
  {
?>
  
	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info">
		    <span class="copy-right-text"><?php echo $copyright; ?> | <a href="<?php echo esc_url( __( 'https://www.sanyog.in/', 'magazie-minimal' ) ); ?>"><?php printf( esc_html__( 'Designed with love by: %1$s.', 'magazie-minimal' ), 'Sanyog Shelar' ); ?></a></span>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->

<?php } wp_footer(); ?>
</body>
</html>
