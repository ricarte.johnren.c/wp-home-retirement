<?php
/**
 * magazie-minimal functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package magazie-minimal
 */

if ( ! function_exists( 'magazie_minimal_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function magazie_minimal_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on magazie-minimal, use a find and replace
	 * to change 'magazie-minimal' to the name of your theme in all the template files.
	 */
	
	load_theme_textdomain( 'magazie-minimal' );


	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support('title-tag' );
	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );
	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'magazie-minimal' ),
	) );
	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );
	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'magazie_minimal_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'magazie_minimal_setup' );
/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function magazie_minimal_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'magazie_minimal_content_width', 640 );
}
add_action( 'after_setup_theme', 'magazie_minimal_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function magazie_minimal_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Left Sidebar', 'magazie-minimal' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'magazie-minimal' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Right Sidebar', 'magazie-minimal' ),
		'id'            => 'sidebar-right',
		'description'   => esc_html__( 'Add widgets here.', 'magazie-minimal' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'magazie_minimal_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function magazie_minimal_scripts() {	
	/*google font  */
	wp_enqueue_style( 'magazie-minimal-googleapis', 'https://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700|Roboto:300,400,500,700,900', array(), null );	
	/*Font-Awesome-master*/
	wp_enqueue_style( 'fontawesome-css', get_template_directory_uri() . '/assets/framework/Font-Awesome/css/font-awesome.css', array(), '4.5.0' );	
	/*Bootstrap CSS*/
	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/assets/framework/bootstrap/css/bootstrap.css', array(), '3.3.7' );	
	/*Style CSS*/
	wp_enqueue_style( 'magazie-minimal-style', get_stylesheet_uri(),'',1.0 );
	/*Bootstrap JS*/
	wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/assets/framework/bootstrap/js/bootstrap.js', array('jquery'), '3.3.7', true );	
	/*navigation JS*/
	wp_enqueue_script( 'magazie-minimal-navigation', get_template_directory_uri() . '/assets/js/navigation.js', array('jquery'), '20151215', true );	
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'magazie_minimal_scripts' );
/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';
/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';
/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';
/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';
/**
 * Load Recent Post widget File.
 */
require get_template_directory() . '/inc/custom-widget/recent-post-widget.php';
/**
 * Change submit Text of comment button.
 */
function magazie_minimal_change_comment_form_submit_label($arg) {
	$arg['label_submit'] = esc_html__('Submit Comment', 'magazie-minimal');
	return $arg;
}
add_filter('comment_form_defaults', 'magazie_minimal_change_comment_form_submit_label');
/**
 * define size of logo.
 */
function magazie_minimal_custom_logo_setup() {
	add_theme_support( 'custom-logo', array(
		'height'      => 75,
		'width'       => 250,
		'flex-width' => true,
	) );
}
add_action( 'after_setup_theme', 'magazie_minimal_custom_logo_setup' );

function magazie_minimal_the_custom_logo() {
	if ( function_exists( 'the_custom_logo' ) ) {
		the_custom_logo();
	}
}

/**
 * enqueue Script for admin dashboard.
 */

if (!function_exists('magazie_minimal_widgets_backend_enqueue')) :
	function magazie_minimal_widgets_backend_enqueue($hook)
	{
		if ('widgets.php' != $hook) {
			return;
		}
		wp_register_script('magazie-minimal-custom-widgets', get_template_directory_uri() . '/assets/js/widgets.js', array('jquery'), true);
		wp_enqueue_media();
		wp_enqueue_script('magazie-minimal-custom-widgets');
	}
	add_action('admin_enqueue_scripts', 'magazie_minimal_widgets_backend_enqueue');
endif;
