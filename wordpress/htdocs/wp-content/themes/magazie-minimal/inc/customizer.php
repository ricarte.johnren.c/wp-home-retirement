<?php
/**
 * magazie-minimal Theme Customizer.
 *
 * @package magazie-minimal
 */


/**
 * Sanitizing the select callback example
 *
 * @since magazie-minimal 1.0.0
 *
 * @see sanitize_key()               https://developer.wordpress.org/reference/functions/sanitize_key/
 * @see $wp_customize->get_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/get_control/
 *
 * @param $input
 * @param $setting
 * @return $choices output
 *
 */
if ( !function_exists('magazie_minimal_sanitize_select') ) :
    function magazie_minimal_sanitize_select( $input, $setting ) {

        // Ensure input is a slug.
        $input = sanitize_key( $input );

        // Get list of choices from the control associated with the setting.
        $choices = $setting->manager->get_control( $setting->id )->choices;

        // If the input is a valid key, return it; otherwise, return the default.
        return ( array_key_exists( $input, $choices ) ? $input : $setting->default );
    }
endif;

/**
 * Sidebar layout options
 *
 * @since magazie-minimal 1.0.0
 *
 * @param null
 * @return array $magazie-minimal_sidebar_layout
 *
 */
if ( !function_exists('magazie_minimal_sidebar_layout') ) :
    function magazie_minimal_sidebar_layout() {
        $magazie_minimal_sidebar_layout =  array(
            'right-sidebar' => __( 'Right Sidebar', 'magazie-minimal'),
            'left-sidebar'  => __( 'Left Sidebar' , 'magazie-minimal'),
            'both-sidebar'  => __( 'Both Sidebar' , 'magazie-minimal'),
            'no-sidebar'    => __( 'No Sidebar', 'magazie-minimal')
        );
        return apply_filters( 'magazie_minimal_sidebar_layout', $magazie_minimal_sidebar_layout );
    }
endif;


/**
 *  Default Theme options
 *
 * @since magazie-minimal 1.0.0
 *
 * @param null
 * @return array $magazie-minimal_theme_layout
 *
 */
if ( !function_exists('magazie_minimal_default_theme_options') ) :
    function magazie_minimal_default_theme_options() {

        $default_theme_options = array(
           'magazie-minimal-footer-copyright'=> esc_html__('Your Own Copyright Text','magazie-minimal'),
           'magazie-minimal-layout'=>'both-sidebar',
           'magazie-minimal-facebook-link'=>"",
           'magazie-minimal-twitter-link'=>"",
           'magazie-minimal-google-plus-link'=>"",
           'magazie-minimal-linkedin-link'=> "",
           'magazie-minimal-dribbble-link'=>"",
            
        );

        return apply_filters( 'magazie_minimal_default_theme_options', $default_theme_options );
    }
endif;



/**
 *  Get theme options
 *
 * @since magazie-minimal 1.0.0
 *
 * @param null
 * @return array magazie-minimal_theme_options
 *
 */
if ( !function_exists('magazie_minimal_get_theme_options') ) :
    function magazie_minimal_get_theme_options() {

        $magazie_minimal_default_theme_options = magazie_minimal_default_theme_options();
        

        $magazie_minimal_get_theme_options = get_theme_mod( 'magazie_minimal_theme_options');
        if( is_array( $magazie_minimal_get_theme_options )){
            return array_merge( $magazie_minimal_default_theme_options, $magazie_minimal_get_theme_options );
        }
        else{
            return $magazie_minimal_default_theme_options;
        }

    }
endif;

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function magazie_minimal_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'refresh';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'refresh';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	/*adding sections for footer options*/
    $wp_customize->add_section( 'magazie-minimal-footer-option', array(
        'priority'       => 170,
        'capability'     => 'edit_theme_options',
        'theme_supports' => '',
        'title'          => __( 'Footer Option', 'magazie-minimal' )
    ) );

    /*copyright*/
    $wp_customize->add_setting( 'magazie_minimal_theme_options[magazie-minimal-footer-copyright]', array(
        'capability'        => 'edit_theme_options',
        'default'           => '',
        'sanitize_callback' => 'wp_kses_post'
    ) );
    $wp_customize->add_control( 'magazie-minimal-footer-copyright', array(
        'label'     => __( 'Copyright Text', 'magazie-minimal' ),
        'section'   => 'magazie-minimal-footer-option',
        'settings'  => 'magazie_minimal_theme_options[magazie-minimal-footer-copyright]',
        'type'      => 'text',
        'priority'  => 10
    ) );


   
    /*defaults options*/
    $defaults = magazie_minimal_get_theme_options();
       
    /**
     * Load customizer custom-controls
     */
    require get_template_directory() . '/inc/customizer-inc/custom-controls.php';

     /**
     * Load Site Layout
     */
    require get_template_directory() . '/inc/customizer-inc/site-layout-section.php';

    /**
     * Load Social Link section
     */
    require get_template_directory() . '/inc/customizer-inc/social-link-section.php';

    }
add_action( 'customize_register', 'magazie_minimal_customize_register' );


/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function magazie_minimal_customize_preview_js() {
	wp_enqueue_script( 'magazie_minimal_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20151216', true );
}
add_action( 'customize_preview_init', 'magazie_minimal_customize_preview_js' );
