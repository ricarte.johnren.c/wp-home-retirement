<?php
/*adding sections for category selection for promo section in homepage*/
$wp_customize->add_section( 'magazie-minimal-site-layout', array(
    'priority'       => 160,
    'capability'     => 'edit_theme_options',
    'title'          => __( 'Design Layout', 'magazie-minimal' )
) );

/* feature cat selection */
$wp_customize->add_setting( 'magazie_minimal_theme_options[magazie-minimal-layout]', array(
    'capability'		=> 'edit_theme_options',
    'default'			=> $defaults['magazie-minimal-layout'],
    'sanitize_callback' => 'magazie_minimal_sanitize_select'
) );

$choices = magazie_minimal_sidebar_layout();
$wp_customize->add_control('magazie_minimal_theme_options[magazie-minimal-layout]',
            array(
            'choices'   => $choices,
            'label'		=> __( 'Select Design Layout', 'magazie-minimal'),
            'section'   => 'magazie-minimal-site-layout',
            'settings'  => 'magazie_minimal_theme_options[magazie-minimal-layout]',
            'type'	  	=> 'select',
            'priority'  => 10
         )
    );



