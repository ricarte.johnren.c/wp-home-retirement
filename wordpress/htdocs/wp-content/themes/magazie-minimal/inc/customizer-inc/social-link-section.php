<?php
/*adding selection for social-link section*/
$wp_customize->add_section( 'magazie-minimal-social-link', array(
    'priority'       => 160,
    'capability'     => 'edit_theme_options',
    'title'          => __( 'Social Link Section', 'magazie-minimal' ),
    'description'    => __( 'Insert Full Social Link  Url Below.', 'magazie-minimal' )
) );

/* Social Link selection */
$wp_customize->add_setting( 'magazie_minimal_theme_options[magazie-minimal-facebook-link]', array(
    'capability'		=> 'edit_theme_options',
    'default'			=> $defaults['magazie-minimal-facebook-link'],
    'sanitize_callback' => 'esc_url_raw'
) );

$wp_customize->add_control( 'magazie-minimal-facebook-link', array(
        'label'     => __( 'Facebook Link', 'magazie-minimal' ),
        'section'   => 'magazie-minimal-social-link',
        'settings'  => 'magazie_minimal_theme_options[magazie-minimal-facebook-link]',
        'type'      => 'url',
        'priority'  => 10
    ) );

/* Social Link selection */
$wp_customize->add_setting( 'magazie_minimal_theme_options[magazie-minimal-twitter-link]', array(
    'capability'        => 'edit_theme_options',
    'default'           => $defaults['magazie-minimal-twitter-link'],
    'sanitize_callback' => 'esc_url_raw'
) );

$wp_customize->add_control( 'magazie-minimal-twitter-link', array(
        'label'     => __( 'Twitter Link', 'magazie-minimal' ),
        'section'   => 'magazie-minimal-social-link',
        'settings'  => 'magazie_minimal_theme_options[magazie-minimal-twitter-link]',
        'type'      => 'url',
        'priority'  => 11
    ) );

/* Social Link selection */
$wp_customize->add_setting( 'magazie_minimal_theme_options[magazie-minimal-google-plus-link]', array(
    'capability'        => 'edit_theme_options',
    'default'           => $defaults['magazie-minimal-google-plus-link'],
    'sanitize_callback' => 'esc_url_raw'
) );

$wp_customize->add_control( 'magazie-minimal-google-plus-link', array(
        'label'     => __( 'Google Plus Link', 'magazie-minimal' ),
        'section'   => 'magazie-minimal-social-link',
        'settings'  => 'magazie_minimal_theme_options[magazie-minimal-google-plus-link]',
        'type'      => 'url',
        'priority'  => 12
    ) );

/* Social Link selection */
$wp_customize->add_setting( 'magazie_minimal_theme_options[magazie-minimal-linkedin-link]', array(
    'capability'        => 'edit_theme_options',
    'default'           =>  $defaults['magazie-minimal-linkedin-link'],
    'sanitize_callback' => 'esc_url_raw'
) );

$wp_customize->add_control( 'magazie-minimal-linkedin-link', array(
        'label'     => __( 'Linkedin Link', 'magazie-minimal' ),
        'section'   => 'magazie-minimal-social-link',
        'settings'  => 'magazie_minimal_theme_options[magazie-minimal-linkedin-link]',
        'type'      => 'url',
        'priority'  => 13
    ) );


/* Social Link selection */
$wp_customize->add_setting( 'magazie_minimal_theme_options[magazie-minimal-dribbble-link]', array(
    'capability'        => 'edit_theme_options',
    'default'           => $defaults['magazie-minimal-dribbble-link'],
    'sanitize_callback' => 'esc_url_raw'
) );

$wp_customize->add_control( 'magazie-minimal-dribbble-link', array(
        'label'     => __( 'Dribbble Link', 'magazie-minimal' ),
        'section'   => 'magazie-minimal-social-link',
        'settings'  => 'magazie_minimal_theme_options[magazie-minimal-dribbble-link]',
        'type'      => 'url',
        'priority'  => 14
    ) );




/*adding set Footer Copy Right Text*/
$wp_customize->add_section( 'magazie-minimal-copy-right-text', array(
    'priority'       => 160,
    'capability'     => 'edit_theme_options',
    'title'          => __( 'Footer Copy Right Text Section', 'magazie-minimal' ),
    'description'    => __( 'Insert Footer Copy Right Text Below.', 'magazie-minimal' )
) );



/* Footer Copy Right Section */
$wp_customize->add_setting( 'magazie_minimal_theme_options[magazie-minimal-footer-copyright]', array(
    'capability'        => 'edit_theme_options',
    'default'           => $defaults['magazie-minimal-footer-copyright'],
    'sanitize_callback' => 'wp_kses_post'
) );

$wp_customize->add_control( 'magazie-minimal-footer-copyright', array(
        'label'     => __( 'Footer Copy Right Text', 'magazie-minimal' ),
        'section'   => 'magazie-minimal-copy-right-text',
        'settings'  => 'magazie_minimal_theme_options[magazie-minimal-footer-copyright]',
        'type'      => 'text',
        'priority'  => 14
    ) );

