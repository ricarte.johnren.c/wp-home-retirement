<?php

class Magazie_Minimal_Recent_Post_Widget extends WP_Widget{
     public function __construct(){
          parent::__construct(
               'magazie-minimal-recent-post-widget',
               __( 'Magazie Minimal Recent Post Widget', 'magazie-minimal' ),
               array( 'description' => __( 'This widgets is used to display recent post with image', 'magazie-minimal' ) )
          );
     }



     public function widget( $args, $instance ){
          extract( $args );
           if(!empty($instance)){
           
            $title = apply_filters( 'widget_title', !empty( $instance['title'] ) ? esc_html( $instance['title'] ) : '', $instance, $this->id_base);

            ?>
            <h2 class="widget-title"><span><?php echo  $title;?></span></h2>
            <?php
             $noofpost= absint($instance['no_of_post'])-1;
             $query_args =array('post_type'=>'post','posts_per_page'=>$noofpost,'order'=>'desc');
             $recent_posts = new WP_Query($query_args);
             if ( $recent_posts->have_posts() ) {
                  while ( $recent_posts->have_posts() ) {
                      $recent_posts->the_post();
            ?>
              <section  class="widget">
                <?php if(has_post_thumbnail()) { ?>
                   <a href="<?php the_permalink(); ?>" target="_blank">
                     <figure>
                          <?php the_post_thumbnail( 'small', array( 'class' => 'aligncenter' ) ); ?>
                      </figure>
                   </a>
                <?php } ?> 
                 <a href="<?php the_permalink(); ?>" target="_blank"> <h2 class="widget-title"><span><?php the_title();?></span></h2> </a>
                 <p><?php the_excerpt(); ?></p>
                  <a class="readmore"href="<?php the_permalink(); ?>"><?php echo esc_html('Read More','magazie-minimal') ?></a>

                
              </section>
         <?php
          }
         wp_reset_postdata();
         }
       }
     }

     public function update( $new_instance, $old_instance ){
          $instance = $old_instance;
          $instance['title'] = sanitize_text_field($new_instance['title']);
          $instance['no_of_post'] = absint( $new_instance['no_of_post'] );
         
          return $instance;
     }

     public function form($instance ){
          ?>
             <p>

                 <label for="<?php echo  esc_attr($this->get_field_id('title')); ?>"><?php _e( 'Title', 'magazie-minimal' ); ?></label><br />
                 <input type="text" name="<?php echo  esc_attr( $this->get_field_name('title')); ?>" id="<?php echo  esc_attr( $this->get_field_id('title')); ?>" value="<?php
                  if (isset($instance['title']) && $instance['title'] != '' ) :
                    echo esc_attr($instance['title']);
                   endif;

                   ?>" class="widefat" />
             </p>
              <p>
                 <label for="<?php echo  esc_attr( $this->get_field_id('no_of_post')); ?>"><?php _e( 'Number of posts to show:', 'magazie-minimal' ); ?></label><br />
                 <input type="number" name="<?php echo  esc_attr( $this->get_field_name('no_of_post')); ?>" id="<?php echo  esc_attr( $this->get_field_id('no_of_post')); ?>" value="<?php 
                   if (isset($instance['no_of_post']) && $instance['no_of_post'] != '' ) :
                    echo esc_html( $instance['no_of_post'] ); 
                    else :
                      echo "2";

                 endif;
                 ?>" class="widefat" />
                 <span class="small"></span>
              </p>
          <?php
     }
}

add_action( 'widgets_init', 'magazie_minimal_recent_post_widget' );
function magazie_minimal_recent_post_widget(){     
    register_widget( 'Magazie_Minimal_Recent_Post_Widget' );
}















