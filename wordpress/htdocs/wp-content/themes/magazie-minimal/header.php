<?php

/**

 * The header for our theme.

 *

 * This is the template that displays all of the <head> section and everything up until <div id="content">

 *

 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials

 *

 * @package magazie-minimal

 */

$GLOBALS['magazie_minimal_theme_options']  = magazie_minimal_get_theme_options();

global $magazie_minimal_theme_options;

$facebook= $magazie_minimal_theme_options['magazie-minimal-facebook-link'];

$twitter= $magazie_minimal_theme_options['magazie-minimal-twitter-link'];

$googleplus= $magazie_minimal_theme_options['magazie-minimal-google-plus-link'];

$linkedin= $magazie_minimal_theme_options['magazie-minimal-linkedin-link'];

$dribbble= $magazie_minimal_theme_options['magazie-minimal-dribbble-link'];



?><!DOCTYPE html>

<html <?php language_attributes(); ?> class="boxed">

<head>

<meta charset="<?php bloginfo( 'charset' ); ?>">

<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

<div id="page" class="site container">

	<div class="row">

		<div class="col-sm-12 col-md-12 page-pd">

	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'magazie-minimal' ); ?></a>

	<header id="masthead" class="site-header" role="banner">
		<div class="header-left-bg">
				<a class="menu-icons" data-toggle="modal" data-target="#menu"><i class="fa fa-bars"></i></a>
				<div class="site-branding">

			<?php

			    $custom_logo_id = get_theme_mod( 'custom_logo' );

				 if(has_custom_logo())

			    { ?>

                <div class="magazie-minimal-logo">

				  <?php the_custom_logo();?>

			    </div>


			 <?php  }

			 else{

			if ( is_front_page() && is_home() && empty($custom_logo_id)) :


			 ?>

				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>

			<?php else : ?>

				<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>

			<?php

		      endif;

              }

			$description = get_bloginfo( 'description', 'display' );

			if ( $description || is_customize_preview() ) : ?>

				<p class="site-description"><?php echo $description; /* WPCS: xss ok. */ ?></p>

			<?php

			endif; ?>



		</div><!-- .site-branding -->

			<?php
			  if(!empty($facebook) || !empty($twitter) || !empty($linkedin) || !empty($dribbble) || !empty($googleplus))
                {
			?>	
					<div class="social-icons text-right">

						<div class="social-area">

							<span><?php esc_html_e('Follow Us On','magazie-minimal'); ?></span>

							<ul class="social-icons">
	                           <?php
	                           	if(!empty($facebook))
	                           	{
	                           ?> 
									<li class="facebook"><a href="<?php echo esc_url($facebook) ?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
	                         <?php 
	                            } 
	                           	if(!empty($twitter))
	                           	{
	                           ?>
									<li class="twitter"><a href="<?php echo esc_url($twitter) ?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
	                         <?php
	                            }
	                             if(!empty($googleplus))
	                            {
	                         ?>
									<li class="google"><a href="<?php echo esc_url($googleplus) ?>" target="_blank"><i class="fa fa-google"></i></a></li>
	                         <?php
	                            }
	                          if(!empty($linkedin))
	                          {

	                          ?>
								<li class="linkedin"><a href="<?php echo esc_url($linkedin) ?>" target="_blank"><i class="fa fa-linkedin"></i></a></li>

						     <?php 
						      }
	                          if(!empty($dribbble))
	                          {
						     ?>		

								<li class="dribbble"><a href="<?php echo esc_url($dribbble) ?>" target="_blank"><i class="fa fa-dribbble"></i></a></li>
	                        <?php
	                          }
	                        ?>
							</ul>

						</div>

					</div>
		   <?php } ?>		

			<div class="modal fade menu-modal" id="menu" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

			<div class="modal-dialog modal-sm" role="document">

				<div class="modal-content">

					<div class="modal-header">

						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

						<h4 class="modal-title" id="myModalLabel"><?php esc_html_e('Menu','magazie-minimal') ?></h4>

					</div>

					<div class="modal-body">

						<nav id="site-navigation" class="main-navigation" role="navigation">

							<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><i class="fa fa-align-justify"></i></button>

							<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>

						</nav><!-- #site-navigation -->

					</div>

				</div>

			</div>

		</div>
		</div>
	</header><!-- #masthead -->

<div class="col-sm-3 pull-right right-sidebar-border">
			<?php get_sidebar('right'); ?>
</div>
		

 <div id="content" class="site-content">


