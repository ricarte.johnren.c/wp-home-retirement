=== Magazie-Minimal ===

Contributors: codexdemon
Tags: two-columns, three-columns, left-sidebar, right-sidebar, custom-background, custom-colors, custom-menu, featured-images, theme-options, threaded-comments, translation-ready, blog
Requires at least: 4.3
Tested up to: 4.8.2
Stable tag: 1.0
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html


== Description ==

Magazie Minimal is a simple and easy to use, modern and creative, user friendly and elegant, WordPress theme for blog, news and magazine sites. It is responsive, cross browser compatible, SEO friendly theme with lots of highly created features. Theme comes with sidebar options. It is very light weighted theme based on customizer. Added custom widgets for Popular Post, Recent Post  and Recommend. Try magazie-minimal today and blog away. Our dedicated support team will guide you in every steps while using theme. 

=== Installation ===

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Frequently Asked Questions ==

Activating Both Sidebar
1> Open Appearance > Customiser > Design Layout > Choose Both SideBar
2> Now Add Widgets to your Right Side Bar Appearance > Customiser > Widgets > Right Sidebar alternative is Direct from Widget Sections

How to Activate Tree Menu in Left Side Bar
1> Open Appearance > Customiser > Widgets > Left Sidebar
2> Add Categories Widgets give title to your menu and select Show post counts and Show hierarchy

== Theme Options ==
1> Social Link Section
2> Design Layout
3> Footer Copy Right Text

== Credits ==

The magazie-minimal WordPress Theme, Copyright 2017 Sanyog Shelar
The magazie-minimal is distributed under the terms of the GNU GPL

All other resources and theme elements are licensed under the [GNU GPL](http://www.gnu.org/licenses/gpl-2.0.txt), version 2 or later.

* Based on Underscores http://underscores.me/, (C) 2012-2016 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)
* 'Roboto Slab' and 'Roboto' Google Fonts - Apache License, version 2.0
* Bootstrap CSS and Java Script http://getbootstrap.com/ [MIT](http://opensource.org/licenses/MIT) Copyright 2011-2015 Twitter, Inc.
* Font-Awesome CSS and Fonts https://github.com/FortAwesome/Font-Awesome FontAwesome 4.6.3 Copyright 2012 Dave Gandy Font License: SIL OFL 1.1 Code License: MIT License http://fontawesome.io/license/

== Image Credit == 
* https://pixabay.com/en/amusement-park-ride-carnival-2456905/

== Changelog ==

= 1.0- Oct 05 2017 =
* Minor Correction and Initial Version.


= 0.9 - Oct 05 2017 =
* Remove Unwanted Codes As per WordPress.org Std.

= 0.5 - Jul 10 2017 =
* Remove Unwanted Codes As per WordPress.org Std.

= 0.4 - 07 Sep 2017 =
* Bug Fixed
* UI Improved

= 0.3 - Jul 2017 =
* Css Improved

= 0.2 - Jul 2017 =
* Bug Fixed

= 0.1 - Jun 2017 =
* Initial release